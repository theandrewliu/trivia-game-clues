import random

from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from .categories import CategoryOut
# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()

class Clue(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool

    class Config:
        orm_mode = True

class Clues(BaseModel):
    page_count: int
    clues: list[Clue]

class Message(BaseModel):
    message: str

@router.get("/api/clue/{clue_id}",response_model=Clue,responses={404: {"model": Message}})
def get_clue(clue_id:int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, c.id, c.title, c.canon 
                FROM clues
                INNER JOIN categories as c ON (clues.category_id = c.id)
                WHERE clues.id = %s
                """,[clue_id],
            )
            clue = cur.fetchone()
            return({
                "id": clue[0],
                "answer": clue[1],
                "question":clue[2],
                "value":clue[3],
                "invalid_count":clue[4],
                "category": {
                    "id":clue[6],
                    "title":clue[7],
                    "canon":clue[8],
                },
                "canon":clue[5],
            })

@router.get("/api/random-clue",response_model=Clue,responses={404: {"model": Message}})
def get_random_clue(valid:bool=True):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            if valid == True:
                cur.execute(
                f"""
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, c.id, c.title, c.canon 
                FROM clues
                INNER JOIN categories as c ON (clues.category_id = c.id)
                WHERE clues.canon = %s AND clues.invalid_count = 0
                ORDER BY random()
                """,[valid]
                )
            else:
                cur.execute(
                f"""
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, c.id, c.title, c.canon 
                FROM clues
                INNER JOIN categories as c ON (clues.category_id = c.id)
                ORDER BY random()
                """
                )
            clue = cur.fetchone()
            return({
                "id": clue[0],
                "answer": clue[1],
                "question":clue[2],
                "value":clue[3],
                "invalid_count":clue[4],
                "category": {
                    "id":clue[6],
                    "title":clue[7],
                    "canon":clue[8],
                },
                "canon":clue[5],
            })

@router.get("/api/clues",response_model=Clue,responses={404: {"model": Message}})
def get_clues(value:int | None = None, page:int=0):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            if value is not None:
                cur.execute(
                    f"""
                    SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, c.id, c.title, c.canon 
                    FROM clues
                    INNER JOIN categories as c ON (clues.category_id = c.id)
                    WHERE clues.value = %s
                    LIMIT 100 OFFSET %s
                    """,[value,page*100]
                )
            else:
                cur.execute(
                    f"""
                    SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, c.id, c.title, c.canon 
                    FROM clues
                    INNER JOIN categories as c ON (clues.category_id = c.id)
                    LIMIT 100 OFFSET %s
                    """,[page*100]
                )
            clues = cur.fetchall()
            results = []
            for clue in clues:
                result={
                    "id": clue[0],
                    "answer": clue[1],
                    "question":clue[2],
                    "value":clue[3],
                    "invalid_count":clue[4],
                    "category": {
                        "id":clue[6],
                        "title":clue[7],
                        "canon":clue[8],
                    },
                    "canon":clue[5],
                }
                results.append(result)
            return results
            
@router.delete("/api/clues/{clue_id}",response_model=Clue,responses={400:{"model":Message}},)
def remove_clue(clue_id: int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(
                    """
                    UPDATE clues
                    SET invalid_count = invalid_count + 1
                    where id = %s;
                    """, [clue_id],
                )
                cur.execute(
                    f"""
                    SELECT cats.id, cats.title, cats.canon, clues.id, clues.question, clues.answer, clues.value, clues.invalid_count, clues.canon, clues.category_id
                    FROM categories AS cats
                    INNER JOIN clues ON (cats.id = clues.category_id)
                    WHERE clues.id = %s;
                    """, [clue_id],
                )
                row = cur.fetchone()
                return {
                "id": row[3],
                "question": row[4],
                "answer": row[5],
                "value": row[6],
                "invalid_count": row[7],
                "canon": row[8],
                "category": {
                  "id": row[0],
                  "title": row[1],
                  "canon": row[2],
                }
                }
            except:
                return {
                    "message": "Unsuccessful"
                }