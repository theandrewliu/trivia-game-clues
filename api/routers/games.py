from datetime import datetime
import string
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from .clues import Clue

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()

class Game(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool
    total_won: int

class CustomGame(BaseModel):
    id: int
    created_on: datetime
    clues: list[Clue]

@router.get("/api/games/{game_id}", response_model=Game)
def get_game(game_id:int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute("""
            SELECT games.id, games.episode_id, games.aired, games.canon, SUM(c.value) AS total_won
            FROM games
            LEFT OUTER JOIN clues as c ON (c.game_id = games.id)
            WHERE games.id = %s
            GROUP BY games.episode_id, games.id, games.aired, games.canon
            """,[game_id])
            game = cur.fetchone()
            return({
                "id":game[0],
                "episode_id":game[1],
                "aired":game[2],
                "canon":game[3],
                "total_won":game[4],
            })

@router.post("/api/custom-games",response_model=CustomGame)
def create_custom_game():
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT cats.id, cats.title, cats.canon,
                    clues.id, clues.question, clues.answer,
                    clues.value, clues.invalid_count,
                    clues.canon, clues.category_id
                FROM categories AS cats
                INNER JOIN clues ON (cats.id = clues.category_id)
                WHERE clues.invalid_count = 0 AND cats.canon = true
                ORDER BY RANDOM()
                LIMIT 30;
                """
            )
            clues = cur.fetchall()
            with conn.transaction():
                cur.execute(
                    """
                    INSERT INTO game_definitions (created_on)
                    VALUES (CURRENT_TIMESTAMP);
                    """
                )
                cur.execute(
                    """
                    SELECT gd.id, gd.created_on
                    FROM game_definitions gd
                    ORDER BY gd.created_on DESC
                    LIMIT 1;
                    """
                )
                new_game_definition = cur.fetchone()
                new_game_definition_id = new_game_definition[0]

                for clue in clues:
                    current_clue_id = clue[0]
                    cur.execute(
                        """
                        INSERT INTO game_definition_clues (game_definition_id, clue_id)
                        VALUES (%s, %s)
                        """,
                        [new_game_definition_id, current_clue_id],
                    )
                return {
                    "id": new_game_definition_id,
                    "created_on": new_game_definition[1],
                    "clues":
                    [{
                        "id": clue[3],
                            "question": clue[4],
                            "answer": clue[5],
                            "value": clue[6],
                            "invalid_count": clue[7],
                            "canon": clue[8],
                            "category": {
                            "id": clue[0],
                            "title": clue[1],
                            "canon": clue[2],
                            }
                        } for clue in clues]

                    }

@router.get(
  "/api/custom-games/{custom_game_id}",
  response_model = CustomGame
)
def get_custom_game(custom_game_id: int):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      cur.execute(
        f"""
        SELECT gd.id, gd.created_on, gdc.game_definition_id, gdc.clue_id, c.id, c.answer,
        c.question, c.value, c.invalid_count, cats.id, cats.title, cats.canon, c.canon
        FROM game_definitions gd
        JOIN game_definition_clues gdc ON (gd.id=gdc.game_definition_id)
        JOIN clues c ON (gdc.clue_id = c.id)
        JOIN categories cats ON (c.category_id = cats.id)
        WHERE gd.id = %s;
        """, [custom_game_id]
      )
      rows = cur.fetchall()
      row = rows[0]
      game_info = {
        "id": row[0],
        "created_on": row[1],
        "clues": [{
             "id": row[4],
                "answer": row[5],
                "question": row[6],
                "value": row[7],
                "invalid_count": row[8],
                "canon": row[12],
                "category": {
                  "id": row[9],
                  "title": row[10],
                  "canon": row[11],
                }
              } for row in rows]
      }
      return game_info

@router.get(
  "/api/custom-games"
)
def get_custom_game():
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      cur.execute(
      """
      SELECT *
      FROM game_definitions;
      """
      )
      rows = cur.fetchall()
      cur.execute(
        """
        SELECT COUNT(*) FROM game_definitions;
        """
      )
      count = cur.fetchone()[0]
      return(
        {
          "count": count,
          "custom-games":
        [{
          "id": row[0],
          "created_on": row[1]
        } for row in rows]}
      )
